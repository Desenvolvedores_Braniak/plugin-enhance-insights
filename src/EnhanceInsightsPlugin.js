import React from "react";
import { VERSION } from "@twilio/flex-ui";
import { FlexPlugin } from "flex-plugin";

import reducers, { namespace } from "./states";
import { getSupervisor } from "./components/helpers/manager";

const PLUGIN_NAME = "EnhanceInsightsPlugin";

const supervisor = getSupervisor();

export default class EnhanceInsightsPlugin extends FlexPlugin {
  constructor() {
    super(PLUGIN_NAME);
  }

  /**
   * This code is run when your plugin is being started
   * Use this to modify any UI components or attach to the actions framework
   *
   * @param flex { typeof import('@twilio/flex-ui') }
   * @param manager { import('@twilio/flex-ui').Manager }
   */
  init(flex, manager) {
    this.registerReducers(manager);

    if (supervisor) {
      console.log(`Selected supervisor: "${supervisor}"`);
      flex.Actions.addListener("afterAcceptTask", (payload) => {
        const { attributes } = payload.task;
        if (!("conversations" in attributes)) attributes["conversations"] = {};
        attributes["conversations"]["campaign"] = supervisor;

        payload.task
          .setAttributes(attributes)
          .then(() => console.log(`Using supervisor: "${supervisor}"`))
          .catch(console.error);
      });
    }
  }

  /**
   * Registers the plugin reducers
   *
   * @param manager { Flex.Manager }
   */
  registerReducers(manager) {
    if (!manager.store.addReducer) {
      // eslint: disable-next-line
      console.error(
        `You need FlexUI > 1.9.0 to use built-in redux; you are currently on ${VERSION}`
      );
      return;
    }

    manager.store.addReducer(namespace, reducers);
  }
}
