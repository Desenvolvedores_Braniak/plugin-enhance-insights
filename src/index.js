import * as FlexPlugin from 'flex-plugin';
import EnhanceInsightsPlugin from './EnhanceInsightsPlugin';

FlexPlugin.loadPlugin(EnhanceInsightsPlugin);
