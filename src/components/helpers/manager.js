import { Manager } from "@twilio/flex-ui";

export const getInstance = () => {
  return Manager.getInstance();
};

export const getSupervisor = () => {
  return getInstance().workerClient.attributes.agent_attribute_1;
};
