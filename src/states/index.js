import { combineReducers } from "redux";

// Register your redux store under a unique namespace
export const namespace = "enhance-insights";

// Combine the reducers
export default combineReducers({});
